import grpc
from user_pb2 import GetUserRequest, CreateUserRequest
from user_pb2_grpc import UserServiceStub

def run():
    channel = grpc.insecure_channel("localhost:50051")
    stub = UserServiceStub(channel)
    # Call the GetUser method
    response = stub.GetUser(GetUserRequest(id=1))
    print("GetUser response:", response.user)
    # Call the CreateUser method
    response = stub.CreateUser(CreateUserRequest(name="Jane Doe", email="jane@example.com"))
    print("CreateUser response:", response.user)

if __name__ == "__main__":
    run()