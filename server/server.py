import grpc
from concurrent import futures
from user_pb2 import GetUserResponse, CreateUserResponse
from user_pb2_grpc import add_UserServiceServicer_to_server, UserServiceServicer

class UserService(UserServiceServicer):
    def GetUser(self, request, context):
        # Perform database query or some other logic to retrieve user information
        # For this example, we'll just return some hardcoded data
        user = GetUserResponse(user={"id": 1, "name": "John Doe", "email": "john@example.com"})
        return user

    def CreateUser(self, request, context):
        # Perform database query or some other logic to create user
        # For this example, we'll just return some hardcoded data
        user = CreateUserResponse(user={"id": 3, "name": request.name, "email": request.email})
        return user

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_UserServiceServicer_to_server(UserService(), server)
    server.add_insecure_port("[::]:50051")
    server.start()
    print("Server started on port 50051")
    server.wait_for_termination()

if __name__ == "__main__":
    serve()
